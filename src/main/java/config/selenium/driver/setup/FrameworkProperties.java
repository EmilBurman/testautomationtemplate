package config.selenium.driver.setup;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

/**
 * Created by Emilbu on 2017-03-20.
 */
public class FrameworkProperties {
    private final Properties configProp = new Properties();

    private FrameworkProperties()
    {
        //Private constructor to restrict new instances
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("config.properties");
        System.out.println("Read all properties from file");
        try {
            configProp.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Bill Pugh Solution for singleton pattern ( No I didn't write this myself)
    private static class LazyHolder
    {
        private static final FrameworkProperties INSTANCE = new FrameworkProperties();
    }

    public static FrameworkProperties getInstance()
    {
        return LazyHolder.INSTANCE;
    }

    //Methods to fetch from config file
    public String getProperty(String key){
        return configProp.getProperty(key);
    }

    public Set<String> getAllPropertyNames(){
        return configProp.stringPropertyNames();
    }

    public boolean containsKey(String key){
        return configProp.containsKey(key);
    }

    //Hardcoded methods
    public String getOperatingSystem(){
        return System.getProperty("os.name");
    }
    public String getProjectPath(){
        return System.getProperty("user.dir");
    }
}
