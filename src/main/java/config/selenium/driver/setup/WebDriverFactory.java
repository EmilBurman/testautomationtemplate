package config.selenium.driver.setup;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;


public class WebDriverFactory {
    private static final long IMPLICIT_WAIT_TIMEOUT = 5;

    private static ThreadLocal<WebDriverFactory> instance = new ThreadLocal<WebDriverFactory>() {
        @Override
        protected WebDriverFactory initialValue() {
            return new WebDriverFactory();
        }
    };

    public static WebDriverFactory getInstance() {
        return instance.get();
    }

    //Variables shared by the whole class
        private WebDriver driver = null;
        boolean operatingSystem = FrameworkProperties.getInstance().getOperatingSystem().startsWith("Windows");
        Path dynamicDirectory = Paths.get(FrameworkProperties.getInstance().getProjectPath(), "src", "main", "java", "config", "selenium", "driver", "setup");

    public WebDriver startDriver(String platform, String browsers) {
        //Checks which platform should be initiated
        switch (platform.toLowerCase()) {
            case "android":
                try {
                    driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), getAppiumCapabilities("android"));
                    return driver;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                break;
            case "ios":
                //iOS support will be added here in the future (hopefully.probably.maybe)
                break;
            case "": {
               return browserFactory(browsers);
            }
            default:
                throw new IllegalStateException("Unsupported platform type, check if platform config is set correctly in the xml.");
        }
        throw new IllegalStateException("Switch failed somehow, not sure what to do here. Ask someone smart to help you");
    }


    public WebDriver getDriver() // call this method to get the driver object and launch the browser
    {
        if (driver != null) {
            driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT_TIMEOUT, TimeUnit.SECONDS);
            return driver;
        } else {
            throw new IllegalStateException("Driver has not been initialized. " +
                    "Please call the startDriver method before using this method");
        }
    }


    public void finishBrowser() // Quits the driver and closes the browser
    {
        getInstance().driver.close();
        getInstance().driver.quit();
    }

    private static DesiredCapabilities getAppiumCapabilities(String mobileOperatingSystem) {
        switch (mobileOperatingSystem.toLowerCase()) {
            case "android":
                DesiredCapabilities androidCapabilities = new DesiredCapabilities();
                androidCapabilities.setBrowserName("Browser");
                androidCapabilities.setCapability("VERSION", "6.0");
                androidCapabilities.setCapability("deviceName", "Emulator");
                androidCapabilities.setCapability("platformName", "Android");
                androidCapabilities.setCapability("appPackage", "com.android.browser");
                androidCapabilities.setCapability("appActivity", "com.android.browser.BrowserActivity");
                androidCapabilities.setCapability("screenShotFormat", "png");
                return androidCapabilities;
            case "ios":
                //These are all dummy values, please reformat and make these correct in the future...
                DesiredCapabilities iPhoneCapabilities = new DesiredCapabilities();
                iPhoneCapabilities.setBrowserName("Browser");
                iPhoneCapabilities.setCapability("VERSION", "6.0");
                iPhoneCapabilities.setCapability("deviceName", "Emulator");
                iPhoneCapabilities.setCapability("platformName", "iPhone");
                iPhoneCapabilities.setCapability("appPackage", "com.iOS.browser");
                iPhoneCapabilities.setCapability("appActivity", "com.iOS.browser.BrowserActivity");
                iPhoneCapabilities.setCapability("screenShotFormat", "png");
                return iPhoneCapabilities;
            default:
                throw new IllegalStateException("Platform is not supported");
        }
    }

    private WebDriver browserFactory(String browsers) {
        String driverPath;
        switch (browsers.toLowerCase()) {
            case "chrome":
                //Checks if windows or mac
                if (operatingSystem) {
                    driverPath = "/WebDrivers/chromedriver.exe";
                } else {
                    driverPath = "/WebDrivers/chromedriver";
                }
                //Sets where to find driver
                System.setProperty("webdriver.chrome.driver", dynamicDirectory + driverPath);
                if (System.getProperty("webdriver.chrome.driver") == null) {
                    throw new IllegalStateException("System variable 'webdriver.chrome.driver' should be set to path for executable driver");
                }
                //Sets the driver
                driver = new ChromeDriver(DesiredCapabilities.chrome());
                //Returns driver
                return driver;

            case "firefox":
                if (operatingSystem) {
                    driverPath = "/WebDrivers/geckodriver.exe";
                } else {
                    driverPath = "/WebDrivers/geckodriver";
                }
                System.setProperty("webdriver.gecko.driver", dynamicDirectory + driverPath);
                driver = new FirefoxDriver(DesiredCapabilities.firefox());
                return driver;

            case "ie11":
                if (operatingSystem) {
                    driverPath = "/WebDrivers/IEDriverServer.exe";
                } else {
                    driverPath = "/WebDrivers/IEDriverServer";
                }
                System.setProperty("webdriver.ie.driver", dynamicDirectory + driverPath);
                DesiredCapabilities capsExplorer = DesiredCapabilities.internetExplorer();
                capsExplorer.setVersion("11");
                driver = new InternetExplorerDriver(DesiredCapabilities.internetExplorer());
                return driver;

            case "safari":
                //Not supported at the moment
                driver = new SafariDriver(DesiredCapabilities.safari());
                return driver;

            case "edge":
                if (operatingSystem) {
                    driverPath = "/WebDrivers/MicrosoftWebDriver.exe";
                } else {
                    driverPath = "/WebDrivers/MicrosoftWebDriver";
                }
                System.setProperty("webdriver.edge.driver", dynamicDirectory + driverPath);
                driver = new EdgeDriver(DesiredCapabilities.edge());
                return driver;

            case "headless":
                if (operatingSystem) {
                    driverPath = "/WebDrivers/phantomjs.exe";
                } else {
                    driverPath = "/WebDrivers/phantomjs";
                }
                DesiredCapabilities capsPhantom = new DesiredCapabilities();
                capsPhantom.setJavascriptEnabled(true);
                capsPhantom.setCapability("takesScreenshot", true);
                capsPhantom.setCapability(
                        PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                        dynamicDirectory + driverPath
                );
                driver = new PhantomJSDriver(capsPhantom);
                return driver;

            default:
                throw new IllegalStateException("Unsupported browser type");
        }
    }
}
