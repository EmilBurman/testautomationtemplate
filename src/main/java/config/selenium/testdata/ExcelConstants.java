package config.selenium.testdata;

import config.selenium.driver.setup.FrameworkProperties;

public class ExcelConstants {
    public static final String Username = "example_user12345\n";

    public static final String Password = "Test@example12345\n";

    public static final String Path_TestData = FrameworkProperties.getInstance().getProjectPath()+"/src/main/java/config/selenium"+"/testdata/";

    public static final String File_TestData = "testData.xlsx";
}
