package config.selenium.utils;


import javax.swing.*;

/**
 * Created by eburman on 2017-01-04.
 */
public class DialogPromptUtils {
    public static String freeTextPrompt() {

        // a jframe here isn't strictly necessary, but it makes the example a little more real
        JFrame frame = new JFrame("Freetext JFrame");

        // prompt the user to enter their name
        String name = JOptionPane.showInputDialog(frame, "What is the captcha?");

        // get the user's input. note that if they press Cancel, 'name' will be null (which is not handled)
        System.out.printf("The captcha is '%s'.\n", name);

        return name;
    }
}
