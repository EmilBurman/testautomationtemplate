package config.selenium.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by eburman on 2017-01-11.
 */
public class PageNavigationUtils {

    private static PageNavigationUtils navigation;

    public static PageNavigationUtils getNavigation() {
        if (navigation == null) {
            navigation = new PageNavigationUtils();
        }
        return navigation;
    }

    public void scrollToElement(WebElement x, WebDriver driver) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", x);
    }
}
