package config.selenium.utils;

import config.selenium.driver.setup.WebDriverFactory;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by eburman on 2017-01-03.
 */
public class ScreenshotUtils {

    /**
     * Method for screenshot taking. It is empty now, because you could save your screenshot as you want.
     * This method calls in tests listeners on test fail
     */

    public static void takeScreenShot(String failedMethod) {
        String dynamicDirectory = "";
        WebDriver driver = WebDriverFactory.getInstance().getDriver();
        System.out.println("ScreenShot method called");

        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        //Set directory for file
        String destDir = dynamicDirectory + "/screenshots/failures";

        //To capture screenshot.
        DateFormat timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss");

        //To create folder to store screenshots
        new File(destDir);

        //Set file name with combination of test method name + date time.
        String destFile = failedMethod + " - " + timestamp.format(new Date()) + ".png";

        try {
            //Store file at destination folder location
            FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
