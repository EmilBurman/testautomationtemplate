package config.selenium.utils;


import java.util.Random;

/**
 * Populate fields with random names
 */
public class NameUtils {

    /**
     * Name length in characters
     *
     * @param nameLength to change max characters in name
     */

    public static String randomName(int nameLength) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < nameLength; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();
        return output;
    }


}
