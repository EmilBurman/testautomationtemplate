package test.selenium.pages.wikipedia;

import config.selenium.driver.setup.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.NoSuchElementException;

/**
 * Created by Emil on 2016-12-22.
 */
public class WikipediaLoginPage extends BasePage {
    @FindBy(id = "firstHeading")
    private WebElement loginHeader;

    @FindBy(name = "wpName")
    private WebElement usernameField;

    @FindBy(name = "wpPassword")
    private WebElement passwordField;

    @FindBy(id = "wpLoginAttempt")
    private WebElement loginButton;

    public WikipediaLoginPage() {
        super(true);
    }

    @Override
    protected void openPage() {
        //Do nothing
    }

    @Override
    public boolean isPageOpened() {
        try {
            return passwordField.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void enterUserName(String username){
        usernameField.clear();
        usernameField.sendKeys(username);
    }

    public void enterPassword(String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public void clickLoginButton() {
        loginButton.click();
    }

}
