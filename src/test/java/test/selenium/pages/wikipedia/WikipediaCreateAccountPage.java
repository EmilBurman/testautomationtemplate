package test.selenium.pages.wikipedia;

import config.selenium.driver.setup.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.NoSuchElementException;

/**
 * Created by eburman on 2017-01-04.
 */
public class WikipediaCreateAccountPage extends BasePage {
    @FindBy(id = "firstHeading")
    private WebElement caHeader;

    @FindBy(id = "wpName2")
    private WebElement caUsernameField;

    @FindBy(id = "wpPassword2")
    private WebElement caPasswordField;

    @FindBy(id = "wpRetype")
    private WebElement caConfirmPasswordField;

    @FindBy(id = "wpEmail")
    private WebElement caEmailField;

    @FindBy(id = "mw-input-captchaWord")
    private WebElement captchaField;

    @FindBy(id = "wpCreateaccount")
    private WebElement caButton;

    public WikipediaCreateAccountPage() {
        super(true);
    }

    @Override
    protected void openPage() {
        //Do nothing
    }

    @Override
    public boolean isPageOpened() {
        try {
            return caHeader.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void enterUserName(String username) {
        caUsernameField.sendKeys(username);
    }

    public void enterPassword(String password) {
        caPasswordField.sendKeys(password);
    }

    public void reTypePassword(String password) {
        caConfirmPasswordField.sendKeys(password);
    }

    public void enterEmail(String email) {
        caEmailField.sendKeys(email);
    }

    public void enterCaptcha(String captcha) {
        captchaField.sendKeys(captcha);
    }

    public void clickCaButton() {
        caButton.click();
    }
}
