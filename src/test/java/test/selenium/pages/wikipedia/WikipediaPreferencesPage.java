package test.selenium.pages.wikipedia;/**
 * Created by Emilbu on 2017-03-21.
 */

import config.selenium.driver.setup.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WikipediaPreferencesPage extends BasePage {
    private static final String PAGE_URL = "ENTER URL HERE WITH HTTPS";

    @FindBy (id = "firstHeading")
    private WebElement header;

    @FindBy (id = "mw-prefsection-personal-info")
    private WebElement personalInfo;

    @FindBy (css = "#mw-htmlform-info > tbody > tr:nth-child(2) > td.mw-input > a")
    private WebElement userGroups;

    @FindBy (css = "#mw-htmlform-info > tbody > tr:nth-child(3) > td.mw-input > a")
    private WebElement connectedApps;

    @FindBy (css = "#mw-htmlform-info > tbody > tr:nth-child(4) > td.mw-input > a")
    private WebElement userEdits;

    @FindBy (css = "#mw-htmlform-info > tbody > tr:nth-child(5) > td.mw-input")
    private WebElement userRegistrationDate;

    public WikipediaPreferencesPage() {
        super(false);
    }

    @Override
    protected void openPage() {
        // Do nothing
    }

    @Override
    public boolean isPageOpened() {
        return header.isDisplayed();
    }

    public boolean compareUserGroups (String x){
        String value = userGroups.getText().toString();
        System.out.println(value);
        return x.equalsIgnoreCase(value);
    }
    public boolean compareConnectedApps (String x){
        String value = connectedApps.getText().toString();
        System.out.println(value);
        return x.equalsIgnoreCase(value);
    }
    public boolean compareUserEdits (String x){
        String value = userEdits.getText().toString();
        System.out.println(value);
        return x.equalsIgnoreCase(value);
    }
    public boolean compareUserRegistrationDate(String x){
        String value = userRegistrationDate.getText().toString();
        System.out.println(value);
        return x.equalsIgnoreCase(value);
    }

}