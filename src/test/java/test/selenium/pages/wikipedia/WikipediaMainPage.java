package test.selenium.pages.wikipedia;

import config.selenium.driver.setup.BasePage;
import config.selenium.utils.PageNavigationUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Emil on 2016-12-21.
 */
public class WikipediaMainPage extends BasePage {
    private static final String PAGE_URL = "https://en.wikipedia.org/wiki/Main_Page";
    /*
   * Define all the UI elements on the page that will be used
   */
    @FindBy(id = "pt-login")
    private WebElement login;

    @FindBy (id = "pt-preferences")
    private WebElement preferences;

    @FindBy(id = "pt-createaccount")
    private WebElement createAccount;

    @FindBy(id = "mp-topbanner")
    private WebElement topBanner;

    @FindBy(id = "pt-userpage")
    private WebElement userPageLink;

    @FindBy(id = "pt-anonuserpage")
    private WebElement userNotLoggedIn;

    @FindBy(linkText = "Svenska")
    private WebElement swedishVersion;

    @FindBy(id = "huvudsidaintro")
    private WebElement swedishVersionIntro;


    public WikipediaMainPage() {
        super(true);
    }

    @Override
    protected void openPage() {
        getDriver().get(PAGE_URL);
    }

    @Override
    public boolean isPageOpened() {
        return topBanner.isDisplayed();
    }

    public WikipediaCreateAccountPage clickCreateAccount() {
        createAccount.click();
        return new WikipediaCreateAccountPage();
    }

    public WikipediaLoginPage clickLogin() {
        login.click();
        return new WikipediaLoginPage();
    }

    public void clickSwedishVersion() {
        PageNavigationUtils.getNavigation().scrollToElement(swedishVersion,getDriver());
        swedishVersion.click();
    }
    public WikipediaPreferencesPage clickPreferences (){
        preferences.click();
        return new WikipediaPreferencesPage();
    }

    public boolean swedishIsDisplayed() {
        try {
            return swedishVersionIntro.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean userLoggedIn() {
        try {
            return userPageLink.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
