package test.selenium.tests.wikipedia;

import config.selenium.driver.setup.WebDriverFactory;
import config.selenium.testdata.ExcelConstants;
import config.selenium.utils.ExcelUtils;
import org.testng.annotations.*;
import test.selenium.pages.wikipedia.WikipediaMainPage;
import test.selenium.pages.wikipedia.WikipediaLoginPage;
import test.selenium.pages.wikipedia.WikipediaPreferencesPage;

import static org.junit.Assert.assertTrue;

public class LoginCheckPreferences {

    @Parameters({"browser"})
    @BeforeClass
    public void beforeTest(String browser) {
        WebDriverFactory.getInstance().startDriver("", browser);
    }

    @Test
    public void loginOnWikipedia() throws Exception {

        //Used to fetch login information from excel sheet
        ExcelUtils.setExcelFile(ExcelConstants.Path_TestData + ExcelConstants.File_TestData, "Blad1");
        String sUserName = ExcelUtils.getCellData(1, 1);
        String sPassword = ExcelUtils.getCellData(1, 2);

        //Prints the login information to the console
        System.out.println("Username and password is " + sUserName + " and " + sPassword);

        //This part is the login test itself
        WikipediaMainPage wikipediaMainPage = new WikipediaMainPage();
        WikipediaLoginPage loginPage = wikipediaMainPage.clickLogin();
        loginPage.enterUserName(sUserName);
        loginPage.enterPassword(sPassword);
        loginPage.clickLoginButton();
        assertTrue("User did not log in, check their credentials and/or the server logs. " +sUserName+" "+sPassword, wikipediaMainPage.userLoggedIn());
    }

    @Test(dependsOnMethods = "loginOnWikipedia")
    public void changePreferences() throws Exception {

        //Used to fetch login information from excel sheet
        String userGroups = ExcelUtils.getCellData(1, 3);
        String userApps = ExcelUtils.getCellData(1, 4);
        String userEdits = ExcelUtils.getCellData(1, 5);
        String userRegistration = ExcelUtils.getCellData(1, 6);

        //This part is the preference test itself
        WikipediaMainPage wikipediaMainPage = new WikipediaMainPage();
        WikipediaPreferencesPage preferencesPage = wikipediaMainPage.clickPreferences();
        assertTrue("User groups does not match saved data", preferencesPage.compareUserGroups(userGroups));
        assertTrue("User apps does not match saved data", preferencesPage.compareConnectedApps(userApps));
        assertTrue("User edits does not match saved data", preferencesPage.compareUserEdits(userEdits));
        assertTrue("User registration date does not match saved data",  preferencesPage.compareUserRegistrationDate(userRegistration));
    }

    @AfterClass
    public void afterTest() {
        WebDriverFactory.getInstance().finishBrowser();
    }
}
