package test.selenium.tests.wikipedia;

import config.selenium.driver.setup.WebDriverFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import test.selenium.pages.wikipedia.WikipediaMainPage;

/**
 * Created by eburman on 2017-01-11.
 */

public class ChangeLanguage {
    @Parameters({"browser"})
    @BeforeTest
    public void beforeTest(String browser) {
        WebDriverFactory.getInstance().startDriver("", browser);
    }

    @Test(invocationCount = 5)
    public void switchToSwedish() {
        WikipediaMainPage mainPage = new WikipediaMainPage();
        mainPage.clickSwedishVersion();
        mainPage.swedishIsDisplayed();
    }

    @AfterTest
    public void afterTest() {
        WebDriverFactory.getInstance().finishBrowser();
    }
}
