@echo off
echo Searching for Android SDK directory.
for /d %%a in ("%USERPROFILE%\AppData\Local\Android\sdk*") do set sdkDir=%%~fa
if not [%sdkDir%%1]==[] (
echo Found Android SDK directory:
echo %sdkDir%
echo.

echo Setting ANDROID_HOME:
setx /m ANDROID_HOME %sdkDir%
echo.

echo Setting ANDROID_SDK:
setx /m ANDROID_SDK $ANDROID_HOME
echo.

echo Updating Path environment variable:
setx /m Path "%sdkDir%\tools;%sdkDir%\platform-tools;%Path%"
goto pause
) else (
echo Unable to find "%USERPROFILE%\AppData\Local\Android\sdk"
echo.
timout 10
goto exit
)

:pause
pause > nul

:exit
exit