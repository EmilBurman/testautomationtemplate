@echo off
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Creates Maven directory in Program Files			::
:: Extracts Maven zip in Maven directory in Program files	::
:: Deletes Maven zip from Maven directory			::
:: Adds Maven to Path						::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:folderNotExist
if not exist "C:\Program Files\Maven" (
echo Maven folder doesn't exist, creating folder.
mkdir "C:\Program Files\Maven"
goto copyMavenZip
)

:folderExist
if exist "C:\Program Files\Maven" goto folderHasContent

:folderHasContent
echo Checking for content in "C:\Program Files\Maven"
if exist dir /B /S /A-D "C:\Program Files\Maven\*apache-maven-*" (
if exist dir /B /S /A-D "C:\Program Files\Maven\*apache-maven-*.zip" (
goto extract
) else if (
echo Folder "C:\Program Files\Maven" already has content. Will do nothing.
echo.
timeout 10
goto exit
) else (
goto copyMavenZip
))

:copyMavenZip
if exist dir /B /S /A-D %USERPROFILE%\Downloads\*apache-maven-* (
for /f %%i in ('dir /B /S /A-D %USERPROFILE%\Downloads\*apache-maven-*') do set zipPath=%%i
copy "%zipPath%" "C:\Program Files\Maven\"
goto moveCheck
)

:moveCheck
if exist dir /B /S /A-D "C:\Program Files\Maven\*apache-maven-*" (
goto extract
) else (
echo Something went wrong. Unable to find Apache Maven zip file in "C:\Program Files\Maven\"
)

:extract
cd C:\Program Files\Maven\
echo Extracting Maven from zip file.
7z x -aos *apache-maven-*.zip
echo.

echo Removing Maven zip file.
del *apache-maven-*.zip
echo.
goto addMavenToPath

:addMavenToPath
cd *apache-maven-*
set mavenBinPath=%cd%\bin
echo Setting new Path:
setx /m Path "%mavenBinPath%;%Path%
goto pause

:pause
pause > nul

:exit
exit