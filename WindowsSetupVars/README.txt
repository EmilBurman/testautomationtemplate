1. Install Java JDK
2. Run "1_setJavaAndPathAndClasspath.bat" as administrator; right click on the file -> click on "Run as Administrator"
3. Download the Maven zip file from https://maven.apache.org
	Note: The .zip file must be located in your standard Windows "Downloads" folder
4. Run "2_setupMavenAndAddMavenBinToPath.bat" as administrator; right click on the file -> click on "Run as Administrator"
5. Install Android Studio
6. Run "3_setAndroidHomeAndSdkAndAddToPath.bat" as administrator; right click on the file -> click on "Run as Administrator"