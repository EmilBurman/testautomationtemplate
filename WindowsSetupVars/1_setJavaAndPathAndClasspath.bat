@echo off
::::::::::::::::::::::::::::::::::::::::::
:: Creates textFileWithOriginalPath.txt	::
:: Creates resetPathToOriginalPath.bat	::
:: Sets JAVA_HOME			::
:: Sets JRE_HOME			::
:: Sets Path				::
:: Sets Classpath			::
::::::::::::::::::::::::::::::::::::::::::

echo Setting working directory to batch script location.
pushd %~dp0
echo Path to batch script location:
echo %cd%
echo.

:checkIfTextFileExists
if exist textFileWithOriginalPath.txt (
echo textFileWithOriginalPath.txt exists.
echo.
goto checkIfBatFileExists
) else (
goto createTextFile
)

:checkIfBatFileExists
if exist resetPathToOriginalPath.bat (
echo resetPathToOriginalPath.bat exists.
echo.
goto resetWorkingDirectory
) else (
goto createBatFile
)

:createTextFile
echo Creating file: textFileWithOriginalPath.txt
echo %Path%>textFileWithOriginalPath.txt
echo.
goto checkIfTextFileExists

:createBatFile
echo Creating file: resetPathToOriginalPath.bat
echo setx /m Path %Path%>resetPathToOriginalPath.bat
echo.
goto checkIfBatFileExists

:resetWorkingDirectory
echo Resetting working directory.
popd
echo Current working directory:
echo %cd%
echo.
goto findJavaDirectories

:findJavaDirectories
echo Finding Java JDK directory.
for /d %%a in ("C:\PROGRA~1\Java\jdk*") do set jdkDir=%%~fa
echo Java JDK directory:
echo %jdkDir%
echo.
echo Finding Java JRE directory.
for /d %%a in ("C:\PROGRA~1\Java\jre*") do set jreDir=%%~fa
echo Java JRE directory:
echo %jreDir%
echo.
goto setEnvironmentVariables

:setEnvironmentVariables
echo Setting JAVA_HOME environment variable:
setx /m JAVA_HOME "%jdkDir%"
echo.
echo Setting JRE_HOME environment variable:
setx /m JRE_HOME "%jreDir%"
goto setNewPath

:setNewPath
set /p originalPath=<%~dp0\textFileWithOriginalPath.txt

echo Setting new Path:
set newPath=%jdkDir%\bin;C:\Program Files\7-Zip\;%originalPath%
setx /m Path "%newPath%
echo.
echo Setting new CLASSPATH:
set newClasspath=%jdkDir%\lib;%originalPath%
setx /m CLASSPATH "%newClasspath%
echo.

goto pause

:pause
pause > nul

:exit
exit