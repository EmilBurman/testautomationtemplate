@echo off
echo Setting environment variables:
echo.
setx /m JAVA_HOME ";"
setx /m Path ";"
setx /m JRE_HOME ";"
setx /m CLASSPATH ";"

echo.

echo Removing Maven folder
echo.
rmdir /s /q "C:\Program Files\Maven"

echo.
echo Removing Android variables:
setx /m ANDROID_HOME ";"
setx /m ANDROID_SDK ";"

goto exit

:pause
pause > nul

:exit
exit