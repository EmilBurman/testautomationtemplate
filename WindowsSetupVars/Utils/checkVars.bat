@echo off
echo Windows environment variables check:
echo.
echo JAVA_HOME:
echo %JAVA_HOME%
echo.
echo PATH:
echo %PATH%
echo.
echo JRE_HOME:
echo %JRE_HOME%
echo.
echo CLASSPATH:
echo %CLASSPATH%
echo.

echo Java verison check:
java -version
echo.
echo Javac version check:
javac -version
echo.

echo Does Maven folder exist?
if exist "C:\Program Files\Maven" (
echo Yes, folder path: "C:\Program Files\Maven"
) else (
echo No Maven folder was found, expected path: "C:\Program Files\Maven"
)
echo.
echo Maven folder contents:
dir /b "C:\Program Files\Maven" | sort
echo.

echo Android environment variables check:
echo ANDROID_HOME:
echo %ANDROID_HOME%
echo.
echo ANDROID_SDK:
echo %ANDROID_SDK%
echo.

goto pause

:pause
pause > nul

:exit
exit
