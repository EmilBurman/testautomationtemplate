# README #

- - -


* # [Framework setup](https://bitbucket.org/EmilBurman/testautomationtemplate/overview#Framework-setup)
    * ## [Dependencies](## Dependencies)
    * ## [Driver setup](## Appium setup)
    * ## [Appium setup](## Appium setup)
         * ### [Android setup](### Android setup)
    * ## [Allure](## Allure)
    * ## [System variables configuration](##System-variables-configuration)
* # [WebElement Selectors](https://bitbucket.org/EmilBurman/testautomationtemplate/wiki/WebElement%20Selectors)
* # [Automation framework architecture designs](https://bitbucket.org/EmilBurman/testautomationtemplate/wiki/Automation%20framework%20architecture%20designs)
* # [Page object and test case design](https://bitbucket.org/EmilBurman/testautomationtemplate/wiki/Page%20object%20and%20test%20case%20design)

* # [Who do I talk to?](#-Who-do-I-talk-to?-#)
- - -


### What is this repository for? ###

This is a test automation framework built on Selenium with TestNG annotations and Allure reporting. The framework is using a enhanced POM (Page Object Model) design and is entirely built on Java. No previous coding is needed to use the framework itself, rather see how to build your own pages using the established POM design in the wiki. The framework is built to run several apps in parallell, but this feature is still untested. If you are having issues please contact the repo owner.

# Framework setup #

Below is a general view on how the framework is built, showing how tests call the WebDriverFactory to initialize the driver before moving on to call tests that extend the BasePage which in turn fetch the initialized driver.

![Automated test framework diagram.png](https://bitbucket.org/repo/ydpqx6/images/1502024714-Automated%20test%20framework%20diagram.png)

1. Download the repo to a directory of your choosing.
1. Open the project directory from your IDE (IntelliJ is recommended)
1. Go to the config.properties and change the variables to your local environment.
1. Run a test from testng/tests, preferably WikipediaMainPage since it is verified to be working.

Please do not forget to set your system variables as seen below or by using the .bat files provided in the project. If you are having issues please attempt to google the issue before contacting the repo owner.

## Dependencies ##

The below dependencies are not handled by the project and needs to be downloaded separably. 

* Appium
    * Android/ios virtual devices
* Maven 3.1<

## Driver setup ##

The framework supports multiple drivers, please see the list below with the appropriate links to download them to your local environment. If you want to add other drivers to the framework you are free to take a look at the available ones at http://www.seleniumhq.org/download/

If you want to switch browser/driver for a test simply change browser.name to any of the below.

1. Chrome - https://sites.google.com/a/chromium.org/chromedriver/downloads
1. IE11 - https://github.com/SeleniumHQ/selenium/wiki/InternetExplorerDriver
1. Edge - https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/
1. Firefox - https://github.com/mozilla/geckodriver/releases
1. Safari (Potentially) - https://webkit.org/blog/6900/webdriver-support-in-safari-10/
1. PhantomJS (headless) - http://phantomjs.org/download.html

Please add all files to the correct directory so that the framework may find them. If you are using a Mac make sure to download the correct drivers, the framework does a operating system check and as such you do not have to worry about changing any settings.

## Appium setup ##

Appium is optional and the framework can still function without configuring your local environment to support it. If you have little or no experience with automation testing it is suggested to focus on browser automation before continuing with mobile automation. Currently only android testing is supported by the framework, ios will hopefully be added in the future. Download appium @ http://appium.io/ and intel HAXM @ https://software.intel.com/en-us/android/articles/intel-hardware-accelerated-execution-manager. Install both applications.

When performing a mobile test please make sure the appium application is running and press the play button after launching your virtual device or connected the physical through a USB cable. If you need help to set up android testing see the guide below.

### Android setup ###

1. Download Android studio (With SDK) https://developer.android.com/studio/index.html
1. Install both applications
1. Set system variables for android as seen below
1. Open the SDK manager
1. Make sure you install all Android SDK tools and the SDK platform, Google API and Google PIs Intel Atom System Image for the android version desired.
1. Click AVD Manager
1. Create a new Android virtual device (Make sure VM Heap is set to 64 and Host GPU is selected). 1024 is plenty when it comes to memory.
1. Enable android in the automation framework by adding "android" to appium.selection in config.properties (Without "")

## Allure setup ##

Allure is entirely optional and does not need to be configured in order to use the framework. Its function is to create a html file that will give a overview of the performed test suite. It is dependent on Maven 3.1 and higher, and as such Maven needs to be downloaded and added to the local path. Follow the steps below in order to configure and run Allure.

1. Download Maven 3.1 or above
1. Set system variable as seen below
1. In the IDE terminal type "mvn clean test" (Without the "")
1. Run a example test in order to create all necessary classes and logs
1. In the IDE terminal type "mvn site"
1. A new html file named "allure-maven-plugin" has been created under /target/site
1. Open the html file in firefox (Chrome does not work due to not allowing file to file transfers)

In order to create the HTML file the terminal command mvn site needs to be run after each test at the moment.

## System variables configuration ##

** Make sure to set these variables locally: **

* JAVA_HOME = To Java JDK
* JRE_HOME = To Java JRE
* CLASSPATH = to Java jdk/lib

** For Allure support add the following variables: **

* Path = Add Maven/bin to path

** For Appium android support add the follow variables: **

* ANDROID_HOME = To Android SDK (Usually under C:\Users\[YOUR NAME HERE]\AppData\Local\Android\sdk)
* ANDROID_SDK = $ANDROID_HOME
* Path = Add SDK/tools to path
* Path = Add SDK/Platform tools to path

# Who do I talk to? #

* Repo owner or admin

Talk to Emil Burman if you want this repo updated or are having any issues (burman.emil@gmail.com)

* Other community or team contact

Victor Lövgren is in charge of the .bat files if they need to be tweaked contact him directly at victor.lovgren@sogeti.com